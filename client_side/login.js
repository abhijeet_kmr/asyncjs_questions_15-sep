// This is login.js file.
// Created variables for DOM Manipulation.
const loginForm = document.getElementById("login-form");
const emailErrorMsg = document.getElementById("email-error-msg");
const passwordErrorMsg = document.getElementById("password-error-msg");
const submitForm = document.getElementById("login-form-submit");

function validateEmail(email) {
  if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email)) {
    return true;
  }
  return false;
}

function validatePassword(password) {
  if (/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(.{10,15})$/.test(password)) {
    return true;
  }
  return false;
}

// Initialised the click event for Log In button
submitForm.addEventListener("click", (e) => {
  e.preventDefault();
  const email = loginForm.email.value;
  const password = loginForm.password.value;

  if (!validateEmail(email)) {
    return (emailErrorMsg.style.display = "block");
  }
  if (validateEmail(email)) {
    emailErrorMsg.style.display = "none";
  }

  if (!validatePassword(password)) {
    return (passwordErrorMsg.style.display = "block");
  }
  if (validatePassword(password)) {
    passwordErrorMsg.style.display = "none";
  }
  fetch("http://127.0.0.1:3000/login", {
    method: "POST",
    body: JSON.stringify({ email, password }),
    headers: { "Content-Type": "application/json" },
  })
    .then((res) => res.json())
    .then((res) => {
      if (res.success) {
        // window.location = "success.html";
      } else {
        passwordErrorMsg.style.display = "block";
      }
    });
});
