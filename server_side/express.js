const fs = require("fs");
const path = require("path");

// 1.
// Write a function that takes username and password.
// Make sure the function returns a promise.
// Function returns a unique ID for the logged In user.
// Function returns an error object with appropriate failed messages with status codes
// {
//     user not found: 404,
//     Incorrect username/password: 400,
//     Internal Server error: 500          // Any other error
// }

// Add setTimeout and Make sure the function resolves or rejects the data after atleast 4 second.

// The generated ID should be expired after 2 minutes.

const loginFunction = (userName, password) => {
  const expireTime = new Date(new Date().getTime() + 120000);
  const userId = Date.now();
  return new Promise((res, rej) => {
    fs.readFile(
      path.join(__dirname, "login.json"),
      { encoding: "utf-8" },
      (err, data) => {
        if (err) {
          rej({ status: 500, error: "Internal Server Error" });
        } else {
          const newData = JSON.parse(data);
          const items = Object.keys(newData);
          if (items.includes(userName) && password === newData[userName]) {
            res({
              id: userId,
              userName: userName,
              expireTime: expireTime,
              authentication: true,
            });
          } else if (
            items.includes(userName) &&
            password !== newData[userName]
          ) {
            rej({ status: 400, error: "Incorrect username/password" });
          } else {
            rej({ staus: 404, error: "user not found" });
          }
        }
      }
    );
  });
};

loginFunction("firefly@slack.com", "fireplace")
  .then((res) => {
    setTimeout(() => {
      console.log(res);
    }, 4000);
  })
  .catch((err) => {
    console.log(err);
  });

// 2.
// console.log(authenticator);
// Write a function to get information about the user's profile.

// Note: You can only call this function if you are logged In and your ID is not expired.

// This function also returns a promise of the user data or appropriate error code.
// {
//     user not found: 404,
//     UnAuthorized: 403, // Expired
//     UnAuthenticated: 401 // Not signedIn
//     Internal Server Error: 500 // any other error
// }

// The function should take 3 seconds . Use setTimeout to cause delay.

loginFunction("firefly@slack.com", "fireplace")
  .then((res) => {
    setTimeout(() => {
      // console.log(res);
      getUsersProfile(res).then(
        (res) => {
          console.log(res);
        },
        (err) => {
          console.log(err);
        }
      );
    }, 3000);
  })
  .catch((err) => {
    console.log(err);
  });

const getUsersProfile = (user) => {
  const userName = user.userName;
  const expireTime = user.expireTime;
  const currentTime = new Date();
  const authentication = user.authentication;
  return new Promise((res, rej) => {
    fs.readFile(
      path.join(__dirname, "./profile.json"),
      { encoding: "utf-8" },
      (err, data) => {
        if (err) {
          rej({ status: 500, error: "Internal Server Error" });
        } else if (currentTime > expireTime) {
          rej({ UnAuthorized: 403, error: "Session Expired" });
        } else if (authentication === false) {
          rej({ status: 401, error: "Not Signed In" });
        } else {
          const newData = JSON.parse(data);
          const profiles = Object.keys(newData);
          if (profiles.includes(userName)) {
            res(newData[userName]);
          } else {
            rej({ status: 404, error: "User not found" });
          }
        }
      }
    );
  });
};

// 3.
// Write a function to get information about the user's todo's list

// Note: You can only call this function if you are logged In and your ID is not expired.
// The function should take 2 seconds . Use setTimeout to cause delay.

// The function should return list of to-do's for the user who is calling the function.
// or appropriate error message
// {
//     UnAuthorized: 403, // Expired
//     UnAuthenticated: 401 // Not signedIn
//     Internal Server Error: 500 // any other error
// }

loginFunction("firefly@slack.com", "fireplace")
  .then((res) => {
    setTimeout(() => {
      getTodoList(res)
        .then((res) => {
          console.log(res);
        })
        .catch((err) => {
          console.log(err);
        });
    }, 2000);
  })
  .catch((err) => {
    console.log(err);
  });

const getTodoList = (user) => {
  const userName = user.userName;
  const expireTime = user.expireTime;
  const currentTime = new Date();
  const authentication = user.authentication;
  return new Promise((res, rej) => {
    fs.readFile(
      path.join(__dirname, "./todo-list.json"),
      { encoding: "utf-8" },
      (err, data) => {
        if (err) {
          rej({ status: 500, error: "Internal server Error" });
        } else if (currentTime > expireTime) {
          rej({ staus: 403, error: "Session Expired" });
        } else if (authentication === false) {
          rej({ status: 401, error: "Not Signed In" });
        } else {
          const newData = JSON.parse(data);
          const todoList = Object.keys(newData);
          if (todoList.includes(userName)) {
            res(newData[userName]);
          }
        }
      }
    );
  });
};
